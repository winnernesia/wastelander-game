Wastelander Game is a online multiplayer sosial deduction game.. Free For Everyone!!!

#### [High Concept Game (via : Google Slides)](https://docs.google.com/presentation/d/1CMfnwxvvBIDpwatjYAg6OFXDSbjehA7mvIjqyCS2-4w/edit?usp=sharing)
## Features

- Free for Everyone
- Online Multiplayer 
- Task & Mission
- Social Deduction

## Authored by
4210181029 - [Winner Insanjaya](https://gitlab.com/winnernesia)

4210181001 - [Ibrahim Al Fauzan](https://gitlab.com/aimalfauzan)

## Overview Gameplay 

Players will enter the map after the apocalypse, then there will be as many capsules as there are players on the map. This capsule is used as the role of each player's skill.
There will be several different tasks together, but each role will have different tasks according to their expertise.
Each task has a timer as a marker of how long the task should be. Compact coordination is needed to complete the tasks that have been given.
2/10 players will become Gray preventing the other players from completing the task.

## Requirement
 - Internet Connection
 - PC (Windows 10, 8, 7)

## Installation

 1. Download via [itch.io]
 2. Install the game
 3. Enjoy!!!
